Sylwester Mróz 

number of album: 150 218.

**Description**

In this project you can load musics and sounsd in wave format, and you can administer this from keyboard. This project using LWJGL and openAL libraries. In src forlder are addded few sounds to testing this program. If you would add your own musics, this musics must be added in src folder, and must have *.wav extension. In Main class you have a place where you can add this musics in program.

**How to compile project**

This project using LWJGL libay, and it's not default libary in Intellij and Eclipse. Before you run this program, you must add this libary in project.
1) Download this libary from my source. It's name lwjgl-2.9.1.

2) If you using intellij use this tutorial

http://wiki.lwjgl.org/wiki/Setting_Up_LWJGL_with_IntelliJ_IDEA

But if you use eclipse use this tutorial

http://wiki.lwjgl.org/wiki/Setting_Up_LWJGL_with_Eclipse

3) Compile and run this program.

**How to use this program**

This program show only blank window, because it need keyboard.

1) If you would play music press "space".

2) If you would stop music press "s".

3) If you would pause music press "p".

4) If you would turn the next music press key "right".

4) If you would turn the prevorius music press key "left".