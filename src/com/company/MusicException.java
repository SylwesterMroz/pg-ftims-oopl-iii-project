package com.company;

/**
 * Created by Sylwester Mr�z on 12.01.2016.
 */
public class MusicException extends Exception {
    MusicException(String message){
        super(message);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
