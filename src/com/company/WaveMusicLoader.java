package com.company;

import org.lwjgl.BufferUtils;
import org.lwjgl.openal.AL10;
import org.lwjgl.util.WaveData;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

/**
 * Created by Sylwester Mr�z on 12.01.2016.
 * This class load end destroy sound in wave format, using lwjgl and openAL libraries.
 */
public class WaveMusicLoader implements Loader,Destroyer {


    public IntBuffer getBuffer() {
        return buffer;
    }

    public IntBuffer getSource() {
        return source;
    }

    public FloatBuffer getSourcePos() {
        return sourcePos;
    }

    public FloatBuffer getSourceVel() {
        return sourceVel;
    }

    public FloatBuffer getListenerPos() {
        return listenerPos;
    }

    public FloatBuffer getListenerVel() {
        return listenerVel;
    }

    public FloatBuffer getListenerOri() {
        return listenerOri;
    }

    private IntBuffer buffer = BufferUtils.createIntBuffer(1);
    private IntBuffer source = BufferUtils.createIntBuffer(1);
    private FloatBuffer sourcePos = (FloatBuffer)BufferUtils.createFloatBuffer(3).put(new float[]{0.0f,0.0f,0.0f}).rewind();
    private FloatBuffer sourceVel = (FloatBuffer)BufferUtils.createFloatBuffer(3).put(new float[]{0.0f, 0.0f, 0.0f}).rewind();
    private FloatBuffer listenerPos = (FloatBuffer)BufferUtils.createFloatBuffer(3).put(new float[]{0.0f, 0.0f, 0.0f}).rewind();
    private FloatBuffer listenerVel = (FloatBuffer)BufferUtils.createFloatBuffer(3).put(new float[]{0.0f,0.0f,0.0f}).rewind();
    private FloatBuffer listenerOri = (FloatBuffer)BufferUtils.createFloatBuffer(6).put(new float[] { 0.0f, 0.0f, -1.0f,  0.0f, 1.0f, 0.0f }).rewind();

    @Override
    public int load(String filename) throws LoaderException {
        AL10.alGenBuffers(buffer);
        if(AL10.alGetError()!=AL10.AL_NO_ERROR)
            throw new LoaderException("Can not generate buffer in " + filename);

        WaveData waveData = WaveData.create(filename);
        if(waveData!=null){
            AL10.alBufferData( buffer.get(0),waveData.format,waveData.data,waveData.samplerate);
            waveData.dispose();
        }else{
            throw new LoaderException("Can not load " + filename);
        }

        AL10.alGenSources(source);
        if(AL10.alGetError()!=AL10.AL_NO_ERROR)
           throw new LoaderException("Can not generate source in " + filename);

        AL10.alSourcei(source.get(0), AL10.AL_BUFFER,  buffer.get(0) );
        AL10.alSourcef(source.get(0), AL10.AL_PITCH,    1.0f          );
        AL10.alSourcef(source.get(0), AL10.AL_GAIN,     1.0f          );
        AL10.alSource (source.get(0), AL10.AL_POSITION, sourcePos     );
        AL10.alSource (source.get(0), AL10.AL_VELOCITY, sourceVel     );

        if (AL10.alGetError() != AL10.AL_NO_ERROR)
            throw new LoaderException("Can not set source in " + filename);

        AL10.alListener(AL10.AL_POSITION,    listenerPos);
        AL10.alListener(AL10.AL_VELOCITY,    listenerVel);
        AL10.alListener(AL10.AL_ORIENTATION, listenerOri);

        return AL10.AL_TRUE;
    }

    @Override
    public void destroy() {
        AL10.alDeleteSources(source);
        AL10.alDeleteBuffers(buffer);
    }
}
