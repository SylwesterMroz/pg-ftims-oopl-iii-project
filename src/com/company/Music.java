package com.company;

/**
 * Created by Sylwester Mr�z on 12.01.2016.
 */
public interface Music extends Sound,Destroyer {
    void load(String filename) throws MusicException;
}
