package com.company;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.openal.AL;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Main extends JFrame{
    /*JFrame, and FButton won't working with openAl, and i don't know why...*/
/*    private  MusicManager musicManager;
    Main(){
        musicManager = new MusicManager();
        musicManager.add("nuclearexplosion.wav");
        musicManager.add("nosy.wav");

        Container cp = this.getContentPane();
        cp.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));
        JButton btnSound1 = new JButton("Play");
        btnSound1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                musicManager.playSong();
            }
        });
        cp.add(btnSound1);
        btnSound1 = new JButton("Pause");
        btnSound1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                musicManager.pauseSong();
            }
        });
        cp.add(btnSound1);
        btnSound1 = new JButton("Stop");
        btnSound1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                musicManager.stopSong();
            }
        });
        cp.add(btnSound1);
        btnSound1 = new JButton("Next");
        btnSound1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                musicManager.nextSong();
            }
        });
        cp.add(btnSound1);
        btnSound1 = new JButton("Prevorius");
        btnSound1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                musicManager.prevoriusSong();
            }
        });
        cp.add(btnSound1);
        btnSound1 = new JButton("Close");
        btnSound1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AL.destroy();
                System.exit(0);
            }
        });
        cp.add(btnSound1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Test SoundEffct");
        this.pack();
        this.setVisible(true);
    }*/

    public static void main(String[] args) {
        try{
            AL.create();
            Display.setDisplayMode(new DisplayMode(640,480));
            Display.setTitle("new");
            Display.create();
        } catch (LWJGLException le) {
            le.printStackTrace();
            System.exit(1);
        }

        //Create music manager
        MusicManager musicManager = new MusicManager();

        //in this place you can add your own musics, but it must be in src folder, and must have wav extension
        musicManager.add("10_-_Hallowed_Be_Thy_Name.wav");
        musicManager.add("Getting_Away_With_Murder.wav");
        musicManager.add("Metallica_-Battery.wav");
        musicManager.add("nuclearexplosion.wav");
        musicManager.add("nosy.wav");

       // new Main();

        while (!Display.isCloseRequested()){
            while (Keyboard.next()){
                if(Keyboard.isKeyDown(Keyboard.KEY_SPACE)){
                    musicManager.playSong();
                }
                if(Keyboard.isKeyDown(Keyboard.KEY_S)){
                    musicManager.stopSong();
                }
                if(Keyboard.isKeyDown(Keyboard.KEY_P)){
                    musicManager.pauseSong();
                }
                if(Keyboard.isKeyDown(Keyboard.KEY_RIGHT)){
                    musicManager.nextSong();
                }
                if(Keyboard.isKeyDown(Keyboard.KEY_LEFT)){
                    musicManager.prevoriusSong();
                }
                musicManager.run();
            }
            Display.update();
            Display.sync(60);
        }
        musicManager.destroyAllMusic();
        Display.destroy();
        AL.destroy();
        System.exit(0);
    }
}
