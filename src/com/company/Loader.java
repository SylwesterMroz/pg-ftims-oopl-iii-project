package com.company;

/**
 * Created by Sylwester Mr�z on 12.01.2016.
 */
public interface Loader {
    int load(String filename) throws LoaderException;
}