package com.company;

/**
 * Created by Sylwester Mr�z on 18.01.2016.
 */
public interface Destroyer {
    void destroy();
}
