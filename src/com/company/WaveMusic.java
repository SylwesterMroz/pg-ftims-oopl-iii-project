package com.company;

import org.lwjgl.openal.AL10;

/**
 * Created by Sylwester Mr�z on 12.01.2016.
 * This class manage sounds in wave format. Implements Music interface.
 */
public class WaveMusic implements Music {
    @Override
    public void load(String filename) throws MusicException{
        try {
            waveMusicLoader.load(filename);
            source = waveMusicLoader.getSource().get(0);
        }catch (Exception e){
            System.out.println(e);
            throw  new MusicException("Wrong format or file doesn't exist!");
        }
    }

    @Override
    public void play() {
        if(source!=-1) {
            AL10.alSourcePlay(source);
        }
    }

    @Override
    public void pause() {
        if(source!=-1) {
            AL10.alSourcePause(source);
        }
    }

    @Override
    public void stop() {
        if(source!=-1) {
            AL10.alSourceStop(source);
        }
    }

    private WaveMusicLoader waveMusicLoader;

    public WaveMusic(){
        source = -1;
        waveMusicLoader = new WaveMusicLoader();
    }
    public WaveMusic(String filename) throws MusicException{
        source = -1;
        waveMusicLoader = new WaveMusicLoader();
        try {
            load(filename);
        }catch (MusicException e){
            throw e;
        }
    }

    private int source;

    @Override
    public void destroy(){
        waveMusicLoader.destroy();
    }
}
