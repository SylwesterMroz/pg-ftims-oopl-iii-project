package com.company;

import java.util.*;

/**
 * Created by Sylwester Mr�z on 12.01.2016.
 * This class manage musics. Using add function you can add new sound.
 *
 */
public class MusicManager {
    public MusicManager(){
        play = stop = pause = false;
        numberOfSound = 0;
    }

    public int getNumberOfSound(){
        return musics.size();
    }

    public void add(String filename){
        if(!musicName.contains(filename)) {
            try {
                Music music = new WaveMusic(filename);
                musicName.add(filename);
                musics.add(music);
                actualSound = musics.get(numberOfSound);
            }catch (MusicException e){
                System.out.println(e);
            }
        }
    }

    public void nextSong(){
        actualSound.stop();
        ++numberOfSound;
        if(numberOfSound == musics.size()) {
            numberOfSound = 0;
        }
        actualSound = musics.get(numberOfSound);
    }

    public void prevoriusSong(){
        actualSound.stop();
        --numberOfSound;
        if(numberOfSound ==-1){
            numberOfSound = musics.size()-1;
        }
        actualSound = musics.get(numberOfSound);
    }

    public void playSong(){
        play = true;
        stop = false;
        pause = false;
    }

    public void stopSong(){
        stop = true;
        play = false;
        pause = false;
    }

    public void pauseSong(){
        pause = true;
        stop = false;
        play = false;
    }

    public void run(){
        if(pause){
            actualSound.pause();
            pause = false;
        }

        if(stop){
            actualSound.stop();
            stop = false;
        }

        if(play){
            actualSound.play();
            play = false;
        }
    }

    public void destroyAllMusic(){
        for(Music m:musics){
            m.destroy();
        }
    }

    private List<Music> musics = new LinkedList<Music>();
    private Set<String> musicName = new TreeSet<>();

    private int numberOfSound;
    private boolean play, stop, pause;
    /*We need only play,pause and stop function, to manage the musics that we cen using the most primary interface*/
    private Sound actualSound;
}
